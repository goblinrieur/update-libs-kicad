# update-libs-kicad

Here is a script to manage modules to be able to update ALL kicad libs @ once

update-libs-kicad
```bash
#! /usr/bin/bash
echo git submodule update --remote --merge 
git submodule update --remote --merge && echo update kicad OK || exit 1
echo git submodule sync --recursive
git submodule sync --recursive  && echo sync kicad OK || exit 1
```

You need of course a .gitmodule file looking like : 
```
[submodule "kicad-packages3d"]
        path = kicad-packages3d
        url = https://gitlab.com/kicad/libraries/kicad-packages3D.git
[submodule "kicad-symbols"]
        path = kicad-symbols
        url = https://gitlab.com/kicad/libraries/kicad-symbols.git
[submodule "kicad-footprints"]
        path = kicad-footprints
        url = https://gitlab.com/kicad/libraries/kicad-footprints.git
```

# Added my own footprints/symbols lib

[own libraries](./PERSO/)

# Official libs for kicad 6 

[submodule "kicad-footprints"](https://github.com/kicad/kicad-footprints)

[submodule "kicad-packages3d"](https://github.com/kicad/kicad-packages3d)

[submodule "kicad-symbols"](https://github.com/kicad/kicad-symbols)

# Outdated external libs 

[submodule "Adafruit-Eagle-Library.kicad"](https://github.com/ryanfobel/Adafruit-Eagle-Library.kicad.git)

[submodule "ProMicroKiCad"](https://github.com/Biacco42/ProMicroKiCad.git)

[submodule "SparkFun-KiCad-Libraries"](https://github.com/sparkfun/SparkFun-KiCad-Libraries.git)

[submodule "arduino-kicad-library"](https://github.com/Alarm-Siren/arduino-kicad-library.git)

[submodule "digikey-kicad-library"](https://github.com/Digi-Key/digikey-kicad-library.git)

# example of execution 

```
0-2046-48-~/GITLAB $ cd update-libs-kicad/
0-2047-49-~/GITLAB/update-libs-kicad $ ls
kicad-footprints  kicad-packages3d  kicad-symbols  LICENSE  PERSO  README.md  update.sh
0-2048-50-~/GITLAB/update-libs-kicad $ ./update.sh 
git submodule update --init --merge
Chemin de sous-module 'kicad-footprints' : 'cb374ed567a148b8e53b3f89918a2f4ba38dd93f' extrait
Chemin de sous-module 'kicad-packages3d' : '482ac8a61e3aae333d783da68ce057eb63e35cd4' extrait
Chemin de sous-module 'kicad-symbols' : 'fc373907ffc951726f1bbec37d5a20e615eb47f1' extrait
git submodule update --remote --merge
Mise à jour cb374ed56..739836bdf
Fast-forward
 Battery.pretty/BatteryHolder_Keystone_103_1x20mm.kicad_mod                                                         |  36 +++++++++----------
 Battery.pretty/BatteryHolder_Keystone_104_1x23mm.kicad_mod                                                         |  36 +++++++++----------
 Battery.pretty/BatteryHolder_Keystone_105_1x2430.kicad_mod                                                         |  32 ++++++++---------
 Battery.pretty/BatteryHolder_Keystone_106_1x20mm.kicad_mod                                                         |  32 ++++++++---------
 Battery.pretty/BatteryHolder_Keystone_107_1x23mm.kicad_mod                                                         |  32 ++++++++---------
 Battery.pretty/BatteryHolder_Keystone_3001_1x12mm.kicad_mod                                                        |  37 ++++++++++---------
 Connector_Audio.pretty/Jack_3.5mm_CUI_SJ2-3593D-SMT_Horizontal.kicad_mod                                           |  58 ++++++++++++++++++++++++++++++
 Connector_Audio.pretty/Jack_XLR_Neutrik_NC3MAFH-PH_Horizontal.kicad_mod                                            |   2 +-
 Connector_Audio.pretty/Jack_XLR_Neutrik_NC3MAMH-PH_Horizontal.kicad_mod                                            |   2 +-
 Connector_Card.pretty/microSD_HC_Molex_47219-2001.kicad_mod                                                        |  52 +++++++++++++++++++++++++++
 Connector_SATA_SAS.pretty/SAS-mini_TEConnectivity_1888174_Vertical.kicad_mod                                       | 144 ++++++++++++++++++++++++++++++++++++-------------------------------------
 Converter_DCDC.pretty/Converter_DCDC_Silvertel_Ag5810.kicad_mod                                                    | 223 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 Crystal.pretty/Crystal_SMD_Abracon_ABM8AIG-4Pin_3.2x2.5mm.kicad_mod                                                |  43 ++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Bel_FC-203-22_Lateral_P17.80x5.00mm_D1.17mm_Horizontal.kicad_mod                |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Eaton_1A5601-01_Inline_P20.80x6.76mm_D1.70mm_Horizontal.kicad_mod               |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Keystone_3512P_Inline_P23.62x7.27mm_D1.02x2.41x1.02x1.57mm_Horizontal.kicad_mod |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Keystone_3512_Inline_P23.62x7.27mm_D1.02x1.57mm_Horizontal.kicad_mod            |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Keystone_3517_Inline_P23.11x6.76mm_D1.70mm_Horizontal.kicad_mod                 |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Keystone_3518P_Inline_P23.11x6.76mm_D2.44x1.70mm_Horizontal.kicad_mod           |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_100_Inline_P20.50x4.60mm_D1.30mm_Horizontal.kicad_mod                |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_111_Inline_P20.00x5.00mm_D1.05mm_Horizontal.kicad_mod                |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_111_Lateral_P18.80x5.00mm_D1.17mm_Horizontal.kicad_mod               |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_445-030_Inline_P20.50x5.20mm_D1.30mm_Horizontal.kicad_mod            |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_519_Inline_P20.60x5.00mm_D1.00mm_Horizontal.kicad_mod                |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_520_Inline_P20.50x5.80mm_D1.30mm_Horizontal.kicad_mod                |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_521_Lateral_P17.00x5.00mm_D1.30mm_Horizontal.kicad_mod               |  48 +++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Schurter_CQM_Inline_P20.60x5.00mm_D1.00mm_Horizontal.kicad_mod                  |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-5x20mm_Schurter_OG_Lateral_P15.00x5.00mm_D1.3mm_Horizontal.kicad_mod                   |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-6.3x32mm_Littelfuse_102071_Inline_P34.70x7.60mm_D2.00mm_Horizontal.kicad_mod           |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-6.3x32mm_Littelfuse_102_122_Inline_P34.21x7.62mm_D1.98mm_Horizontal.kicad_mod          |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-6.3x32mm_Littelfuse_102_Inline_P34.21x7.62mm_D2.54mm_Horizontal.kicad_mod              |  56 +++++++++++++++++++++++++++++
 Fuse.pretty/Fuseholder_Clip-6.3x32mm_Littelfuse_122_Inline_P34.21x7.62mm_D2.54mm_Horizontal.kicad_mod              |  56 +++++++++++++++++++++++++++++
 Inductor_THT.pretty/L_CommonMode_PulseElectronics_PH9455x105NL_1.kicad_mod                                         |  39 ++++++++++++++++++++
 Inductor_THT.pretty/L_CommonMode_PulseElectronics_PH9455x155NL_1.kicad_mod                                         |  39 ++++++++++++++++++++
 Inductor_THT.pretty/L_CommonMode_PulseElectronics_PH9455x205NL_1.kicad_mod                                         |  39 ++++++++++++++++++++
 Inductor_THT.pretty/L_CommonMode_PulseElectronics_PH9455x405NL_1.kicad_mod                                         |  39 ++++++++++++++++++++
 Inductor_THT.pretty/L_CommonMode_PulseElectronics_PH9455x705NL_1.kicad_mod                                         |  39 ++++++++++++++++++++
 Inductor_THT.pretty/L_CommonMode_PulseElectronics_PH9455xxx6NL_2.kicad_mod                                         |  39 ++++++++++++++++++++
 LED_SMD.pretty/LED_WS2812B-2020_PLCC4_2.0x2.0mm.kicad_mod                                                          |  42 ++++++++++++++++++++++
 LED_SMD.pretty/LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm.kicad_mod                                                        |   8 ++---
 Module.pretty/Arduino_Nano.kicad_mod                                                                               |  11 +++++-
 Module.pretty/Arduino_Nano_WithMountingHoles.kicad_mod                                                             |  11 +++++-
 OptoDevice.pretty/Everlight_ITR9608-F.kicad_mod                                                                    |  60 +++++++++++++++++++++++++++++++
 OptoDevice.pretty/Lightpipe_Bivar_RLP1-400-650.kicad_mod                                                           |  68 +++++++++++++++++++++++++++++++++++
 OptoDevice.pretty/Osram_BP104-SMD.kicad_mod                                                                        |  59 ++++++++++++++++++++++++++++++
 OptoDevice.pretty/Osram_BPW34S-SMD.kicad_mod                                                                       |  59 ++++++++++++++++++++++++++++++
 OptoDevice.pretty/Osram_SFH2430.kicad_mod                                                                          |  59 ++++++++++++++++++++++++++++++
 OptoDevice.pretty/Osram_SFH2440.kicad_mod                                                                          |  59 ++++++++++++++++++++++++++++++
 OptoDevice.pretty/Osram_SMD-DIL2_4.5x4.0mm.kicad_mod                                                               |  54 ----------------------------
 RF_Module.pretty/ESP32-WROOM-32.kicad_mod                                                                          | 119 ++++++++++++++++++++++++++++++++++++-------------------------
 Transformer_SMD.pretty/Transformer_Ethernet_Bourns_PT61020EL.kicad_mod                                             |  64 +++++++++++++++++++++++++++++++++
 Transformer_THT.pretty/Transformer_Wuerth_760871131.kicad_mod                                                      |  48 +++++++++++++++++++++++++
 Varistor.pretty/RV_Rect_V25S440P_L26.5mm_W8.2mm_P12.7mm.kicad_mod                                                  |  38 ++++++++++++++++++++
 53 files changed, 2492 insertions(+), 286 deletions(-)
 create mode 100644 Connector_Audio.pretty/Jack_3.5mm_CUI_SJ2-3593D-SMT_Horizontal.kicad_mod
 create mode 100644 Connector_Card.pretty/microSD_HC_Molex_47219-2001.kicad_mod
 create mode 100644 Converter_DCDC.pretty/Converter_DCDC_Silvertel_Ag5810.kicad_mod
 create mode 100644 Crystal.pretty/Crystal_SMD_Abracon_ABM8AIG-4Pin_3.2x2.5mm.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Bel_FC-203-22_Lateral_P17.80x5.00mm_D1.17mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Eaton_1A5601-01_Inline_P20.80x6.76mm_D1.70mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Keystone_3512P_Inline_P23.62x7.27mm_D1.02x2.41x1.02x1.57mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Keystone_3512_Inline_P23.62x7.27mm_D1.02x1.57mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Keystone_3517_Inline_P23.11x6.76mm_D1.70mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Keystone_3518P_Inline_P23.11x6.76mm_D2.44x1.70mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_100_Inline_P20.50x4.60mm_D1.30mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_111_Inline_P20.00x5.00mm_D1.05mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_111_Lateral_P18.80x5.00mm_D1.17mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_445-030_Inline_P20.50x5.20mm_D1.30mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_519_Inline_P20.60x5.00mm_D1.00mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_520_Inline_P20.50x5.80mm_D1.30mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Littelfuse_521_Lateral_P17.00x5.00mm_D1.30mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Schurter_CQM_Inline_P20.60x5.00mm_D1.00mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-5x20mm_Schurter_OG_Lateral_P15.00x5.00mm_D1.3mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-6.3x32mm_Littelfuse_102071_Inline_P34.70x7.60mm_D2.00mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-6.3x32mm_Littelfuse_102_122_Inline_P34.21x7.62mm_D1.98mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-6.3x32mm_Littelfuse_102_Inline_P34.21x7.62mm_D2.54mm_Horizontal.kicad_mod
 create mode 100644 Fuse.pretty/Fuseholder_Clip-6.3x32mm_Littelfuse_122_Inline_P34.21x7.62mm_D2.54mm_Horizontal.kicad_mod
 create mode 100644 Inductor_THT.pretty/L_CommonMode_PulseElectronics_PH9455x105NL_1.kicad_mod
 create mode 100644 Inductor_THT.pretty/L_CommonMode_PulseElectronics_PH9455x155NL_1.kicad_mod
 create mode 100644 Inductor_THT.pretty/L_CommonMode_PulseElectronics_PH9455x205NL_1.kicad_mod
 create mode 100644 Inductor_THT.pretty/L_CommonMode_PulseElectronics_PH9455x405NL_1.kicad_mod
 create mode 100644 Inductor_THT.pretty/L_CommonMode_PulseElectronics_PH9455x705NL_1.kicad_mod
 create mode 100644 Inductor_THT.pretty/L_CommonMode_PulseElectronics_PH9455xxx6NL_2.kicad_mod
 create mode 100644 LED_SMD.pretty/LED_WS2812B-2020_PLCC4_2.0x2.0mm.kicad_mod
 create mode 100644 OptoDevice.pretty/Everlight_ITR9608-F.kicad_mod
 create mode 100644 OptoDevice.pretty/Lightpipe_Bivar_RLP1-400-650.kicad_mod
 create mode 100644 OptoDevice.pretty/Osram_BP104-SMD.kicad_mod
 create mode 100644 OptoDevice.pretty/Osram_BPW34S-SMD.kicad_mod
 create mode 100644 OptoDevice.pretty/Osram_SFH2430.kicad_mod
 create mode 100644 OptoDevice.pretty/Osram_SFH2440.kicad_mod
 delete mode 100644 OptoDevice.pretty/Osram_SMD-DIL2_4.5x4.0mm.kicad_mod
 create mode 100644 Transformer_SMD.pretty/Transformer_Ethernet_Bourns_PT61020EL.kicad_mod
 create mode 100644 Transformer_THT.pretty/Transformer_Wuerth_760871131.kicad_mod
 create mode 100644 Varistor.pretty/RV_Rect_V25S440P_L26.5mm_W8.2mm_P12.7mm.kicad_mod
Chemin de sous-module 'kicad-footprints' : fusionné dans '739836bdfac4c3ca755f0e08c78bcab861c045c7'
Mise à jour 482ac8a6..45df600c
Fast-forward
 Diode_THT.3dshapes/Diode_Bridge_DIGITRON_KBPC_T.step    | Bin 0 -> 175832 bytes
 Diode_THT.3dshapes/Diode_Bridge_DIGITRON_KBPC_T.wrl     | Bin 0 -> 248603 bytes
 Diode_THT.3dshapes/Diode_Bridge_GeneSiC_KBPC_T.step     | Bin 0 -> 143184 bytes
 Diode_THT.3dshapes/Diode_Bridge_GeneSiC_KBPC_T.wrl      | Bin 0 -> 312511 bytes
 Diode_THT.3dshapes/Diode_Bridge_GeneSiC_KBPC_W.step     | Bin 0 -> 26614 bytes
 Diode_THT.3dshapes/Diode_Bridge_GeneSiC_KBPC_W.wrl      | Bin 0 -> 129789 bytes
 Package_DFN_QFN.3dshapes/Vishay_PowerPAK_MLP32_55G.step | Bin 0 -> 755247 bytes
 Package_DFN_QFN.3dshapes/Vishay_PowerPAK_MLP32_55G.wrl  | Bin 0 -> 324956 bytes
 8 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 Diode_THT.3dshapes/Diode_Bridge_DIGITRON_KBPC_T.step
 create mode 100644 Diode_THT.3dshapes/Diode_Bridge_DIGITRON_KBPC_T.wrl
 create mode 100644 Diode_THT.3dshapes/Diode_Bridge_GeneSiC_KBPC_T.step
 create mode 100644 Diode_THT.3dshapes/Diode_Bridge_GeneSiC_KBPC_T.wrl
 create mode 100644 Diode_THT.3dshapes/Diode_Bridge_GeneSiC_KBPC_W.step
 create mode 100644 Diode_THT.3dshapes/Diode_Bridge_GeneSiC_KBPC_W.wrl
 create mode 100644 Package_DFN_QFN.3dshapes/Vishay_PowerPAK_MLP32_55G.step
 create mode 100644 Package_DFN_QFN.3dshapes/Vishay_PowerPAK_MLP32_55G.wrl
Chemin de sous-module 'kicad-packages3d' : fusionné dans '45df600c5e3dd5113d62e6445115e7c37bdf362f'
Mise à jour fc373907..879023fb
Fast-forward
 74xGxx.kicad_sym                        |    6 +-
 74xx.kicad_sym                          |    2 +-
 Amplifier_Operational.kicad_sym         |   14 +--
 Analog_ADC.kicad_sym                    |    6 +-
 Audio.kicad_sym                         |    2 +-
 Battery_Management.kicad_sym            |   76 +++++++-------
 Comparator.kicad_sym                    |    2 +-
 Connector_Generic.kicad_sym             |   78 +++++++-------
 Connector_Generic_MountingPin.kicad_sym |   78 +++++++-------
 Connector_Generic_Shielded.kicad_sym    |   78 +++++++-------
 Diode.kicad_sym                         |  905 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++---------------------------------------------------------------------------------------------------
 Display_Character.kicad_sym             |    4 +-
 Driver_LED.kicad_sym                    |   95 +++++++++++++++++
 Interface.kicad_sym                     |  147 ++++++++++++++++++++++++++-
 Interface_Ethernet.kicad_sym            |    6 +-
 Interface_Optical.kicad_sym             |    8 +-
 Interface_UART.kicad_sym                |   34 +++----
 Isolator.kicad_sym                      |  379 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++----
 LED.kicad_sym                           |    6 +-
 Memory_RAM.kicad_sym                    |   14 +--
 Power_Management.kicad_sym              |    4 +-
 RF.kicad_sym                            |   12 +--
 RF_Module.kicad_sym                     |   24 ++---
 Regulator_Linear.kicad_sym              |   28 +++---
 Regulator_Switching.kicad_sym           |    6 +-
 Sensor.kicad_sym                        |   63 ++++++++++++
 Sensor_Humidity.kicad_sym               |    4 +-
 Sensor_Optical.kicad_sym                |   32 +++---
 Sensor_Temperature.kicad_sym            |    2 +-
 Switch.kicad_sym                        |    2 +-
 Timer.kicad_sym                         |  225 +++++++++++++++++++++++++++++++++++++++++
 Transformer.kicad_sym                   | 1047 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 Transistor_FET.kicad_sym                |    2 +-
 Valve.kicad_sym                         |   16 +--
 obsolete/MCU_Infineon.dcm               |   12 +--
 obsolete/Memory_RAM.dcm                 |   12 +--
 36 files changed, 2565 insertions(+), 866 deletions(-)
Chemin de sous-module 'kicad-symbols' : fusionné dans '879023fba005d23f285b6d052d9e6bcba1d754aa'
git submodule sync --recursive
Synchronisation de l'URL sous-module pour 'kicad-footprints'
Synchronisation de l'URL sous-module pour 'kicad-packages3d'
Synchronisation de l'URL sous-module pour 'kicad-symbols'
0-2049-51-~/GITLAB/update-libs-kicad $ 
```


# now have a CI to keep artifact as tooling around autoupdate 
