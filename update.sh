#! /usr/bin/bash
#set -x
echo -e "\e[1;31mgit submodule update --init --merge\e[0m"
git submodule update --init --recursive 
echo -e "\e[1;31mgit submodule update --remote --merge\e[0m" 
git submodule update --remote --merge --force 
echo -e "\e[1;31mgit submodule sync --recursive\e[0m"
git submodule sync --recursive 
echo -e "\e[1;31m----END----\e[0m"
echo
#set +x
